import React, { Component } from 'react'
import { Link } from 'react-router-dom';

export class Accueil extends Component {
    render() {
        return (
            <div>
                <div className="pagetitle">
                    <div className="container">
                        <div className="pagetitle-wrapper d-flex justify-content-between align-items-center">
                            <div className="pagetitle-right">
                                Popular Titles
                            </div>

                        </div>

                    </div>
                </div>
                <div className="container">
                    <div className="row py-5">
                        <Link to="/series">
                            <div className="card-wrapper">
                                <img alt="" className="card-img" src="https://raw.githubusercontent.com/Shaita-KrZ/dishop-coding-challenge/master/assets/placeholder.png" />
                                <div className="img-overlay-wrapper">
                                    <p className="img-overlay">SERIES</p>
                                </div>

                                <p>Popular Series</p>
                            </div>
                        </Link>
                        <Link to="/films">
                            <div className="card-wrapper">
                                <img alt="" className="card-img" src="https://raw.githubusercontent.com/Shaita-KrZ/dishop-coding-challenge/master/assets/placeholder.png" />
                                <div className="img-overlay-wrapper">
                                    <p className="img-overlay">MOVIES</p>
                                </div>
                                <p>Popular Movies</p>
                            </div>
                        </Link>


                    </div>

                </div>
            </div>

        )
    }
}

export default Accueil
