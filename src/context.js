import React, { Component } from "react";
const ProductContext = React.createContext();

class AppProvider extends Component {
    state = {
        sample: [],
        test: "test",
        isLoading: false,
        errorMessage: false
    };
    componentDidMount() {
        this.getData()

    }
    getData = () => {
        this.setState({ isLoading: true });
        fetch('json/sample.json')
            .then(response => response.json())
            .then(data => this.setState({ sample: data.entries, isLoading: false }))
            .catch(error => this.setState({ errorMessage: true, isLoading: false }));

    }
    render() {
        return (
            <ProductContext.Provider
                value={{
                    ...this.state
                }}
            >
                {this.props.children}
            </ProductContext.Provider>
        );
    }
}

const AppConsumer = ProductContext.Consumer;

export { AppProvider, AppConsumer };
